# isNum
just some missing number helpers

### Usage:

    var isNum = import('isNum');
    isNum("100"); //true
    isNum.isInt("100"); //true
    isNum.isFloat.isPositive("100.1") //true

### Functions:

 - isNum()
 - isNum.isInt()
 - isNum.isInt.isPositive()
 - isNum.isInt.isNegative()
 - isNum.isFloat()
 - isNum.isFloat.isPositive()
 - isNum.isFloat.isNegative()


### Tests
You can find some tests [here](https://tonicdev.com/nemesarial/isnum)