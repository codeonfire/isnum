/*
    Usage:
        var isNum = import('isNum');
        isNum("100"); //true
        isNum.isInt("100"); //true
        isNum.isFloat.isPositive("100.1") //true
 */
module.exports = (function(){
    var isNum=function(num){return !isNaN(num - 0) && num !== null && num !== "" && typeof num !== 'boolean' && num != false;};

    isNum.isInt=function(num){return isNum(num) && (parseInt(num) == Number(num)); };
    isNum.isInt.isPositive=function(num){return isNum.isInt(num) && Number(num)>0; };
    isNum.isInt.isNegative=function(num){return isNum.isInt(num) && Number(num)<0; };

    isNum.isFloat=function(num){return isNum(num) && (parseFloat(num) == Number(num)); };
    isNum.isFloat.isPositive=function(num){return isNum.isFloat(num) && Number(num)>0; };
    isNum.isFloat.isNegative=function(num){return isNum.isFloat(num) && Number(num)<0; };
    return isNum;    
})();